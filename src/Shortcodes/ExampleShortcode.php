<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Shortcodes\Shortcodes;

use Illuminate\Auth\AuthManager;

final class ExampleShortcode
{
    public function __construct(
        private readonly AuthManager $auth,
    ) {
    }

    // Ver https://github.com/webwizo/laravel-shortcodes
    public function register($shortcode, $content, $compiler, $name, $viewData): string
    {
        return '<div class="example-shortcode">Shortcode de ejemplo</div>';
    }
}
