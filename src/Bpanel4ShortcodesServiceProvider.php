<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Shortcodes;

use Bittacora\Bpanel4\Shortcodes\Shortcodes\ExampleShortcode;
use Illuminate\Http\Request;
use Illuminate\Support\ServiceProvider;
use Shortcode;

final class Bpanel4ShortcodesServiceProvider extends ServiceProvider
{
    public function register(): void
    {
    }

    public function boot(): void
    {
        if(!str_contains($this->app->make(Request::class)->getRequestUri(), '/bpanel/')) {
            \Shortcode::enable();
            // \Shortcode::register('products-grid', ExampleShortcode::class);
        }
    }
}
